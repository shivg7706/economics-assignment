import tkinter as tk
import math
import matplotlib.pyplot as plt


def CompoundAmount():
    print('CompoundAmount')
    # to find : furture worth
    # given : present amount
    N, i, given = topLevel('Present Amount')
    ans = (1 + i) ** N

    return ans

def PresentWorth():
    print('PresentWorth')
    N, i, given = topLevel('Future Amount')

    # to find : present worth
    # given : future amount
    ans = ((1 + i) ** N) ** -1

    return ans

def SinkingFund():
    print('SinkingFund')
    # to find : annuity amount
    # given : future amount
    N, i, given = topLevel('Future Amount')
    
    ans = i / ((1 + i) ** N - 1)

    return ans    

def SeriesCompoundAmount():
    print('SeriesCompound Amount')
    N, i, given = topLevel('Annunity Amounts')
    # to find : future worth 
    # given : annunity amounts
    ans = ((1 + i) ** N -1) / i

    return ans

def SeriesPresentWorth():
    print('SeriesPresent Worth')
    N, i, given = topLevel('Annunity Amounts')
    
    # to find : present worth
    # given : annunity amount

    ans = ((1 + i) ** N - 1)/(i * ((1 + i ) ** N))

    return ans

def CapitalRecovery():
    print('CapitalRecovery')
    N, i, given = topLevel('Present Amount')

    # to find :annunity worth
    # given : present amount

    ans = (i * ((1 + i ) ** N))/((1 + i) ** N - 1)

    return ans

def arithematicGradientConversionFactor():
    ans = 1 # don't know ask Rahul Varshney

def switcher(choice):
    switch = {
        1: CompoundAmount,
        2: PresentWorth,
        3: SinkingFund,
        4: SeriesCompoundAmount,
        5: SeriesPresentWorth,
        6: CapitalRecovery
    }
    return switch.get(choice, 'Wrong Choice')


def ShowChoice():
    func = switcher(v.get())

    if func == 'Wrong Choice':
        print('Nikal Le')
    else:
        func()

def createPanel(root):

    global v
    v = tk.IntVar()
    languages = [
        ("Compound Amount", 1),
        ("Present Worth", 2),
        ("Sinking Fund", 3),
        ("Series Compound Amount", 4),
        ("Series Present Worth", 5),
        ("Capital Recovery", 6)
    ]

    author = 'Arsalan Khan | Shivam Gupta | Tauseef Moin'

    title_bar = tk.Label(root, text='ANNUAL WORTH', fg='#FFFFFF', bg='#20B2AA', font=('Monaco', 40), relief=tk.RAISED)
    check = tk.Label(root, text='Check the button:', justify = tk.LEFT, padx = 20)
    Quit = tk.Button(root, text='QUIT', command=root.destroy, relief=tk.RAISED, width=20, height=2)
    status_bar = tk.Label(root, text=author, bd=1, relief=tk.SUNKEN, font=('Monaco', 15))


    title_bar.pack(side=tk.TOP, fill=tk.X)
    check.pack(fill=tk.X)
    status_bar.pack(side=tk.BOTTOM, fill=tk.X)
    Quit.pack(side=tk.BOTTOM)

    for language in languages:
        temp = tk.Radiobutton(root, 
                    text=language[0],
                    padx = 20,
                    variable=v, 
                    command=ShowChoice,
                    value=language[1],
                    font=("Monaco", 20, "bold"))
        temp.pack()

def topLevel(text):

    top = tk.Toplevel()
    top.title('Input')
    top.geometry('400x100')

    tk.Label(top, text="Time Period (per annum)", anchor='w', font=('Monaco', 15), pady=5).grid(sticky=tk.E)
    tk.Label(top, text="Interest rate", anchor='w', font=('Monaco', 15), pady=5).grid(sticky=tk.E)
    tk.Label(top, text=text, anchor='w', font=('Monaco', 15), pady=5).grid(sticky=tk.E)
    e1 = tk.Entry(top, width=15)
    e2 = tk.Entry(top, width=15)
    e3 = tk.Entry(top, width=15)

    e1.grid(row=0, column=1)
    e2.grid(row=1, column=1)
    e3.grid(row=2, column=1)

    button = tk.Button(root, text='Submit', command=root.destroy, relief=tk.RAISED, width=20, height=2)

    button.grid(sticky=tk.E)

    print(e1.get(), e2.get(), e3.get())
    return e1.get(), e2.get(), e3.get()




def main():
    global root
    root = tk.Tk()
    width = root.winfo_screenwidth()
    height = root.winfo_screenheight()
    root.geometry('{}x{}'.format(width, height))
    root.title('Economics Assignment')
    createPanel(root)

    root.mainloop()

if __name__ == '__main__':
    main()


